/*
 * factorial.h
 *
 *  Created on: 19 Feb 2015
 *      Author: niall
 */

#ifndef INC_FACTORIAL_H_
#define INC_FACTORIAL_H_

int factorial(int n);

#endif /* INC_FACTORIAL_H_ */
