/*
 * factorial.cpp
 *
 *  Created on: 19 Feb 2015
 *      Author: niall
 */
#include "factorial.h"

int factorial(int n) {
        int result = 1;
        for (int i = 1; i <= n; i++) {
                result *= i;
         }
        return result;
}

